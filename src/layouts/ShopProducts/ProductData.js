const ProductData =[
    {
        id:1,
        title: "Product 1",
        price: 100,
        category:"Category1",
        image:""
    },
    {
        id:2,
        title: "Product 2",
        price: 200,
        category:"Category2",
        image:"",
    },
    {
        id:3,
        title: "Product 3",
        price: 300,
        category:"Category5",
        image:""
    },
    {
        id:4,
        title: "Product 3",
        price: 200,
        category:"Category4",
        image:""
    },
    {
        id:5,
        title: "Product 3",
        price: 100,
        category:"Category3",
        image:""
    },
    {
        id:6,
        title: "Product 3",
        price: 30,
        category:"Category1",
        image:""
    },
]

export default ProductData;