import React from "react";
import ProductData from "./ProductData";
import { useState } from "react";
import { Modal, Button } from "antd";

const ShopProducts = () => {
  const [data, setData] = useState(ProductData);
  const filterResult = (catItem) => {
    const result = ProductData.filter((curData) => {
      return curData.category === catItem;
    });
    setData(result);
  };
  const [isModalVisible, setIsModalVisible] = useState(false);
  const showModal = () => {
    setIsModalVisible(true);
  };
  const handleOk = () => {
    setIsModalVisible(false);
  };
  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div>
      <h1 className="text-center text-info">All Products</h1>

      <div className="container-fluid mx-2">
        <div className="row mt-5 mx-2">
          <div className=" col-md-3">
            <button
              className="btn btn-warning w-100 mb-4"
              onClick={() => filterResult("Category1")}
            >
              Category1
            </button>
            <button
              className="btn btn-warning w-100 mb-4"
              onClick={() => filterResult("Category2")}
            >
              Category2
            </button>
            <button
              className="btn btn-warning w-100 mb-4"
              onClick={() => filterResult("Category3")}
            >
              Category3
            </button>
            <button
              className="btn btn-warning w-100 mb-4"
              onClick={() => filterResult("Category4")}
            >
              Category4
            </button>
            <button
              className="btn btn-warning w-100 mb-4"
              onClick={() => filterResult("Category5")}
            >
              Category5
            </button>
            <button
              className="btn btn-warning w-100 mb-4"
              onClick={() => setData(ProductData)}
            >
              All Products
            </button>
          </div>
          <div className="col-md-9 mb-6">
            <div className="row">
              {data.map((values) => {
                const { id, title, price, image, desc } = values;

                return (
                  <>
                    <div className="col-md-4" key={id}>
                      <div className="card">
                        <img src={image} className="card-img-top" alt="..." />
                        <div className="card-body">
                          <h5 className="card-title">{title}</h5>
                          <p>Price: {price}</p>
                          <p className="card-text">
                            Some quick example text to build on the card title
                            and make up the bulk of the card's content.
                          </p>
                          <Button type="primary" onClick={showModal}>
                            QuickView
                          </Button>
                          <Modal
                            title={title}
                            visible={isModalVisible}
                            onOk={handleOk}
                            onCancel={handleCancel}
                          >
                            <p>Price: {price}</p>
                            <p>Description: {desc} </p>
                          </Modal>
                        </div>
                      </div>
                    </div>
                  </>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShopProducts;
