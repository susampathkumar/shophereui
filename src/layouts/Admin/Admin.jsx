import React from "react"
import "./Admin.scss"
import {
  Form,
  Input,
  Button,
  Radio,
  Select,
  Cascader,
  DatePicker,
  InputNumber,
  TreeSelect,
  Switch,
} from 'antd';

import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'


const Admin = () => {
    const {TextArea} = Input;


    return (
      <div className="outer-container">
        <h1>Admin access</h1>

        <div className="add-item-container">
          <h3>Add new item</h3>

          <Form
            labelCol={{ span: 3 }}
            wrapperCol={{ span: 10 }}
            layout="horizontal"
            initialValues={{ size: "default" }}
            size={"default"}
          >
            <Form.Item label="Item Name">
              <Input />
            </Form.Item>
            <Form.Item label="Description">
              <TextArea
                placeholder="Enter product description here..."
                autoSize={{ minRows: 2 }}
              />
            </Form.Item>

            <Form.Item
              name="imageUrl"
              label="Image URL"
              rules={[
                {
                  required: true,
                },
                {
                  type: "url",
                  warningOnly: true,
                },
                {
                  type: "string",
                  min: 6,
                },
              ]}
            >
              <Input placeholder="https://url-of-product-image" />
            </Form.Item>
            <Form.Item
              name="price"
              label="Price"
              rules={[
                {
                  required: true,
                },
                {
                  type: "number",
                  warningOnly: true,
                },
                {
                  type: "number",
                },
              ]}
            >
              <InputNumber placeholder="0.00" />
            </Form.Item>
            <Form.Item
              name="discountedPrice"
              label="Discounted Price"
              rules={[
                {
                  required: false,
                },
                {
                  type: "number",
                },
                {
                  type: "number",
                },
              ]}
            >
              <InputNumber placeholder="0.00" />
            </Form.Item>
            <Form.Item label="Category">
              <Select>
                <Select.Option value="Men">Men</Select.Option>
                <Select.Option value="Women">Women</Select.Option>
                <Select.Option value="Kids">Kids</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Is returnable?"
              valuePropName="checked"
              className="switch-btn"
            >
              <Switch />
            </Form.Item>
            <Form.Item
              label="In stock?"
              valuePropName="checked"
              className="switch-btn"
            >
              <Switch />
            </Form.Item>
            <Form.Item label="Is featured?" valuePropName="checked">
              <Switch />
            </Form.Item>
            <Form.Item>
              <Button label="asd" type="submit">Add item to shop</Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
}

export default Admin