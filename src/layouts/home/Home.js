import React from 'react';
import '../../App.scss';
import Cards from '../../components/cards/Cards';
import HeroSection from '../../components/hero/HeroSection';
import Footer from '../../components/footer/Footer';
import Categories from '../../components/Category/Categories';

function Home() {
    return (
        <>
            <HeroSection />
           
            <div className="container"><Categories /></div>
            <Cards />
            <Footer/>
        </>
    )
}

export default Home;
