import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShopware } from '@fortawesome/free-brands-svg-icons';
import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'
import { Button } from '../button/Button';
import './Navbar.css';
const sidebarNavItems = [
    {
      display: 'Menu',
      icon: <i className='bx bx-home'></i>,
      to: '/',
      section: '',
    },
    {
      display: 'My cart',
      icon: <i className='bx bx-cart'></i>,
      to: '/',
      section: 'cart',
    },
    {
      display: 'Profile',
      icon: <i className='bx bx-user'></i>,
      to: '/',
      section: 'profile',
    },
    {
      display: 'Signout ',
      icon: <i className='bx bx-exit'></i>,
      to: '/',
      section: 'signout',
    },
  ];
  

function Navbar(props) {

    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);
    const isMobile = props.isMobile;
    
    const showButton = () => {
        if (window.innerWidth <= 960) {
            setButton(false);
        }
        else {
            setButton(true)
        }
    }

    window.addEventListener('resize', showButton);

    return (
        <>
            <nav className={isMobile ? 'navbar' : 'navbar active'}>
                <div className='navbar-container'>
                    <Link to='/' className="navbar-logo">
                        ShopHere&nbsp;
                        <FontAwesomeIcon icon={faShopware} />
                    </Link>
                    <div className='menu-icon' onClick={handleClick}>
                        <FontAwesomeIcon icon={click ? faTimes : faBars} />
                    </div>
                </div>
                
{isMobile && <div className={click ? 'nav-menu active' : 'nav-menu'}>
{sidebarNavItems.map((item, index) => (
          <Link to={item.to} key={index}>
            <div
              className='sidebar__menu__item'
            >
              <div className='sidebar__menu__item__icon' >{item.icon}</div>
              <div className='sidebar__menu__item__text'>{item.display}</div>
            </div>
          </Link>
        ))}
                    </div> }
            </nav>
        </>
    );
}

export default Navbar;