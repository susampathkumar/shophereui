import { useEffect, useRef, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import './Sidebar.css';

import { faBars, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShopware } from '@fortawesome/free-brands-svg-icons';
const sidebarNavItems = [
  {
    display: 'Menu',
    icon: <i className='bx bx-home'></i>,
    to: '/',
    section: '',
  },
  {
    display: 'My cart',
    icon: <i className='bx bx-cart'></i>,
    to: '/',
    section: 'cart',
  },
  // {
  //   display: 'Profile',
  //   icon: <i className='bx bx-user'></i>,
  //   to: '/',
  //   section: 'profile',
  // },
  // {
  //   display: 'Signout ',
  //   icon: <i className='bx bx-exit'></i>,
  //   to: '/',
  //   section: 'signout',
  // },
  {
    display: 'SignIn',
    icon: <i className='bx bx-user'></i>,
    to: '/signup',
    section: 'cart',
  },


];

const Sidebar = (props) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [stepHeight, setStepHeight] = useState(0);
  const [mini, setMini] = useState(false);
  const [windowDimension, setWindowDimension] = useState(null);
  const sidebarRef = useRef();
  const indicatorRef = useRef();
  const location = useLocation();

  useEffect(() => {
    setTimeout(() => {
      const sidebarItem = sidebarRef.current.querySelector(
        '.sidebar__menu__item'
      );
      indicatorRef.current.style.height = `${sidebarItem.clientHeight}px`;
      setStepHeight(sidebarItem.clientHeight);
    }, 50);
  }, []);

  // change active index
  useEffect(() => {
    const curPath = window.location.pathname.split('/')[1];
    const activeItem = sidebarNavItems.findIndex(
      (item) => item.section === curPath
    );
    setActiveIndex(curPath.length === 0 ? 0 : activeItem);
  }, [location]);

  const toggleStrikethrough = (e) => {
    e.target.style.width = e.target.style.width == '16rem' ? '6rem' : '16rem';
    // props.setMini(mini);

    setMini(!mini);
    console.log(mini)
  };

  // change view mobile responsively

  useEffect(() => {
    setWindowDimension(window.innerWidth);
  }, []);

  useEffect(() => {
    function handleResize() {
      setWindowDimension(window.innerWidth);
    }

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const isMobile = windowDimension <= 960;

  return (
    <>
      <div
      className='sidebar'
      onClick={toggleStrikethrough}
    >
        
      <div className='sidebar__logo' style={{display: 'flex', justifyContent: 'space-evenly', flexDirection:'row'}}> <Link to='/' className="sidebar__logo">
          {mini ? <h2>ShopHere&nbsp;</h2> : null}
                        <FontAwesomeIcon icon={faShopware} />
                    </Link> </div>
      <div ref={sidebarRef} className='sidebar__menu'>
        <div
          ref={indicatorRef}
          className='sidebar__menu__indicator'
          style={{
            transform: `translateX(-50%) translateY(${
              activeIndex * stepHeight
            }px)`,
          }}
        ></div>
        {sidebarNavItems.map((item, index) => (
          <Link to={item.to} key={index}>
            <div
              className={`sidebar__menu__item ${
                activeIndex === index ? 'active' : ''
              }`}
            >
              <div className='sidebar__menu__item__icon' >{item.icon}</div>
              <div className='sidebar__menu__item__text'>{item.display}</div>
            </div>
          </Link>
        ))}
      </div>
    </div>

    </>
  );
};

export default Sidebar;

// {isMobile ? (<div
//     style={{position:'fixed', width:"100vw",top:0, justifyContent:'center', backgroundColor:'white', marginLeft:0, marginBottom:"1rem"}}
//   >
//     <div ref={sidebarRef}  style={{display:'flex', justifyContent:'space-evenly'}}>
//       {sidebarNavItems.map((item, index) => (
//         <Link to={item.to} key={index} style={{backgroundColor:"white", width:'100$'}}>
//           <div
//           >
//             <div style={{padding: '1rem 1rem',
//               fontSize: '1.75rem',
//               fontWeight: '500',
//               color: '#555555'}}>{item.icon}</div>
//           </div>
//         </Link>
//       ))}
//     </div>
//   </div>)
//   : (
