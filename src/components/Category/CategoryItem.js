
import  styled  from "styled-components";
import data from "../data";


const Container = styled.div`
flex: 1;
margin: 10px;
height: 60vh;
`;

const Image = styled.img`
width: 100%;
height: 100%;
object-fit: cover;`;

const Info = styled.div`

top: 0;
left: 0;
display: flex;
flex-flow: column wrap;
justify-content: flex-start;
align-items: flex-start;
`;

const Title = styled.h1``

const Button = styled.button`
padding: .5rem;
font-size: 1.2rem;
background-color: transparent;
cursor: pointer;`;


const CategoryItem = ({item}) => {
  return (
    <Container>
      <Image src={item.img} />
      <Info>
        <Title>
          <Title>{item.title}</Title>
          <Button>Buy Now</Button>
        </Title>
      </Info>
    </Container>

  )
};

export default CategoryItem;
