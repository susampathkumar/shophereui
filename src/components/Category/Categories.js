
import React from 'react';
import { categories } from '../data';
import CategoryItem from './CategoryItem';
import  styled  from 'styled-components';

const Container = styled.div`
display: flex;
padding: 1rem;
justify-content: space-between;
align-items: center;
min-height: 80vh`;


const Categories = () => {
  return (
      <Container>
          {categories.map(item=>(
              <CategoryItem item={item}/>
          ))}
      </Container>

  )
}

export default Categories;
